// Create a factorial function

const factorial = (n) => {
  if (typeof n !== "number") {
    return false;
  }
  if (n < 0) return undefined;
  if (n === 0) return 1;
  if (n === 1) return 1;
  return n * factorial(n - 1);
};

const names = {
  Eren: {
    name: "Eren Yaeger",
    age: 28,
  },
  Levi: {
    name: "Levi Ackerman",
    age: 38,
  },
};

const div_check = (n) => {
  // if(n % 5 === 0 && n % 7 === 0) return 0;
  if (n % 5 === 0) return 0;
  if (n % 7 === 0) return 0;
  return 1;
};

module.exports = {
  factorial: factorial,
  div_check: div_check,
  names: names,
};