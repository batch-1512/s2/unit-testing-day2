const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");

chai.use(http);

describe("API Test Suite", () => {
  it("Test API Get people is running", () => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("Test API Get people returns 200", () => {
    chai
      .request("http://localhost:5001")
      .get("/people")
      .end((err, res) => {
        expect(res.status).to.equal(200);
      });
  });

  it("Test API Post person returns 400 if no NAME property", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        alias: "Mikasa",
        age: 27,
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post person is running", () => {
    chai
      .request("http://localhost:5001")
      .get("/person")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("Test API Post person returns 400 if no AGE property", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({
        name: "Mikasa",
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it("Test API Post person returns 400 if no ALIAS object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/person")
      .type("json")
      .send({})
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });
});
